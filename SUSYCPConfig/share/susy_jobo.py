import ROOT
ROOT.xAOD.Init().ignore()
ROOT.xAOD.LoadDictionaries().ignore()
from SUSYCPConfig.susytools_config import SUSYToolsConfig
from SUSYCPConfig.output_configuration import output_sequence
from PathResolver.PathResolver import FindCalibFile
jps.AthenaCommonFlags.AccessMode = "ClassAccess"
jps.AthenaCommonFlags.HistOutputs = ["TREE:susy_tree.root"]

from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.AlgSequence import AlgSequence
from SUSYCPConfig.susy_systematics import kinematics


configuration = SUSYToolsConfig()

config_file = "SUSYCPConfig/SUSYTools_MonoH.conf"
tenv = ROOT.TEnv(FindCalibFile(config_file))
configuration.read_tenv(tenv)


athAlgSeq += CfgMgr.CP__SysListLoaderAlg(
    "SysLoaderAlg",
    sigmaRecommended=1
)

ToolSvc += CfgMgr.ST__SUSYObjDef_xAOD(
    "SUSYTools",
    ConfigFile = config_file,
    PRWConfigFiles = [
        "dev/PileupReweighting/mc16_13TeV/pileup_mc16e_dsid410470_FS.root",
        configuration.PRW.ActualMu2018File,
    ],
    PRWLumiCalcFiles = [
        "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
    ],
)

athAlgSeq += CfgMgr.ApplySUSYToolsAlg(SUSYTools=ToolSvc.SUSYTools, OutputPrefix="SUSY", systematicsRegex=".*")

view_electrons = createAlgorithm("CP::AsgViewFromSelectionAlg", "SUSYElectronsViewAlg")
view_electrons.selection= ["passOR,as_char"]
view_electrons.input="SUSYElectrons_%SYS%"
view_electrons.inputRegex = "|".join(kinematics["all"])
view_electrons.output = "SUSYPassORElectrons_%SYS%"
athAlgSeq += view_electrons

view_muons = createAlgorithm("CP::AsgViewFromSelectionAlg", "SUSYMuonsViewAlg")
view_muons.selection= ["passOR,as_char"]
view_muons.input="SUSYMuons_%SYS%"
view_muons.inputRegex = "|".join(kinematics["all"])
view_muons.output = "SUSYPassORMuons_%SYS%"
athAlgSeq += view_muons

view_photons = createAlgorithm("CP::AsgViewFromSelectionAlg", "SUSYPhotonsViewAlg")
view_photons.selection= ["passOR,as_char"]
view_photons.input="SUSYPhotons_%SYS%"
view_photons.inputRegex = "|".join(kinematics["all"])
view_photons.output = "SUSYPassORPhotons_%SYS%"
athAlgSeq += view_photons

view_taus = createAlgorithm("CP::AsgViewFromSelectionAlg", "SUSYTausViewAlg")
view_taus.selection= ["passOR,as_char"]
view_taus.input="SUSYTaus_%SYS%"
view_taus.inputRegex = "|".join(kinematics["all"])
view_taus.output = "SUSYPassORTaus_%SYS%"
athAlgSeq += view_taus

view_jets = createAlgorithm("CP::AsgViewFromSelectionAlg", "SUSYJetsViewAlg")
view_jets.selection= ["passOR,as_char"]
view_jets.input="SUSYJets_%SYS%"
view_jets.inputRegex = "|".join(kinematics["all"])
view_jets.output = "SUSYPassORJets_%SYS%"
athAlgSeq += view_jets

athAlgSeq += output_sequence("SUSYTree", "SUSYPassOR", configuration, "TREE")


print(athAlgSeq)
MessageSvc.defaultLimit = 9999999  # all messages 

