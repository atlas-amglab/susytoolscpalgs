import ROOT
ROOT.xAOD.Init().ignore()
ROOT.xAOD.LoadDictionaries().ignore()
from SUSYCPConfig.susy_configuration import full_sequence
from SUSYCPConfig.output_configuration import output_sequence
from SUSYCPConfig.susytools_config import SUSYToolsConfig
from PathResolver.PathResolver import FindCalibFile
jps.AthenaCommonFlags.AccessMode = "ClassAccess"
jps.AthenaCommonFlags.HistOutputs = ["TREE:cp_tree.root"]

from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.AlgSequence import AlgSequence


configuration = SUSYToolsConfig()

config_file = "SUSYCPConfig/SUSYTools_MonoH.conf"
tenv = ROOT.TEnv(FindCalibFile(config_file))
configuration.read_tenv(tenv)


athAlgSeq += CfgMgr.CP__SysListLoaderAlg(
    "SysLoaderAlg",
    sigmaRecommended=1
)

prw_file = "dev/PileupReweighting/mc16_13TeV/pileup_mc16e_dsid410470_FS.root"

full_seq, syst_patterns = full_sequence(
    "mc", configuration,
    prwFiles=[
        prw_file,
        configuration.PRW.ActualMu2018File
    ],
    lumiCalcFiles=[
        "GoodRunsLists/data18_13TeV/20190708/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]
)
athAlgSeq += full_seq

athAlgSeq += output_sequence("CPAlgTree", "CPPassOR", configuration, "TREE")

print(athAlgSeq)
MessageSvc.defaultLimit = 9999999  # all messages 

