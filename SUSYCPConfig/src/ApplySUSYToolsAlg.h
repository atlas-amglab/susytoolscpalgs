#ifndef SUSYCPCONFIG_APPLYSUSYTOOLSALG_H
#define SUSYCPCONFIG_APPLYSUSYTOOLSALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AsgTools/ToolHandle.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"
#include "SystematicsHandles/SysListHandle.h"

class ApplySUSYToolsAlg: public ::AthAnalysisAlgorithm { 
 public: 
  ApplySUSYToolsAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ApplySUSYToolsAlg(); 

  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  execute();        //per event
 private: 
  ToolHandle<ST::ISUSYObjDef_xAODTool> m_susyTools;
  std::string m_outputPrefix;
  CP::SysListHandle m_systematicsList{this};
  bool m_firstEvent{true};
  std::map<std::string, ST::SystInfo> m_systInfoMap;
  std::vector<CP::SystematicSet> m_systs;
}; 

#endif //> !SUSYCPCONFIG_APPLYSUSYTOOLSALG_H
