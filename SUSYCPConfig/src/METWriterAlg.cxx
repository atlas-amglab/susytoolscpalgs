// SUSYCPConfig includes
#include "METWriterAlg.h"

//#include "xAODEventInfo/EventInfo.h"

#include "xAODMissingET/MissingETContainer.h"



METWriterAlg::METWriterAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
  declareProperty("METTerm", m_metTerm = "FinalTrk");
  declareProperty("METName", m_metName);
  declareProperty("TreeName", m_treeName);
  declareProperty("BranchName", m_branchName = "FinalTrkMet");
}


METWriterAlg::~METWriterAlg() {}


StatusCode METWriterAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  TTree* treePtr = tree(m_treeName.c_str());
  if (!treePtr)
    return StatusCode::FAILURE;

  treePtr->Branch((m_branchName+"_met").c_str(), &m_met);
  treePtr->Branch((m_branchName+"_phi").c_str(), &m_phi);
  return StatusCode::SUCCESS;
}

StatusCode METWriterAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode METWriterAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  const xAOD::MissingETContainer* met = nullptr;
  ATH_CHECK( evtStore()->retrieve(met, m_metName));
  const xAOD::MissingET* metObj = (*met)[m_metTerm];
  if (metObj == nullptr) {
    ATH_MSG_ERROR("No term " << m_metTerm << " in " << m_metName);
    return StatusCode::FAILURE;
  }

  m_met = metObj->met();
  m_phi = metObj->phi();

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode METWriterAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


