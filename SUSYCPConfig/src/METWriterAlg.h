#ifndef SUSYCPCONFIG_METWRITERALG_H
#define SUSYCPCONFIG_METWRITERALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"


class METWriterAlg: public ::AthAnalysisAlgorithm { 
 public: 
  METWriterAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~METWriterAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 
  std::string m_metName;
  std::string m_metTerm;
  std::string m_treeName;
  std::string m_branchName;

  float m_met;
  float m_phi;
}; 

#endif //> !SUSYCPCONFIG_METWRITERALG_H
