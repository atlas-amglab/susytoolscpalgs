
#include "GaudiKernel/DeclareFactoryEntries.h"


#include "../METWriterAlg.h"
DECLARE_ALGORITHM_FACTORY( METWriterAlg )


#include "../ApplySUSYToolsAlg.h"
DECLARE_ALGORITHM_FACTORY( ApplySUSYToolsAlg )

DECLARE_FACTORY_ENTRIES( SUSYCPConfig ) 
{
  DECLARE_ALGORITHM( ApplySUSYToolsAlg );
  DECLARE_ALGORITHM( METWriterAlg );
}
