// SUSYCPConfig includes
#include "ApplySUSYToolsAlg.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowAuxInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include <xAODCore/AuxContainerBase.h>
#include <xAODBase/IParticleHelpers.h>
#include "SystematicsHandles/Helpers.h"
#include "AthContainers/ConstDataVector.h"
#include <memory>

namespace {
  std::ostream& operator<<(std::ostream& os, const ST::SystInfo& info)
  {
    os
      << info.systset.name() << " affects "
      << (info.affectsKinematics ? "kinematics, " : "")
      << (info.affectsWeights ? "weights, " : "")
      << "type enum " << info.affectsType
      << " and weight ids: ";
    for (unsigned int weightID : info.affectedWeights)
      os << weightID << ", ";
    return os;
  }
}

ApplySUSYToolsAlg::ApplySUSYToolsAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
  declareProperty("SUSYTools", m_susyTools);
  declareProperty("OutputPrefix", m_outputPrefix="SUSY");

}


ApplySUSYToolsAlg::~ApplySUSYToolsAlg() {}


StatusCode ApplySUSYToolsAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  ATH_CHECK( m_susyTools.retrieve());
  CP::SystematicSet systs;
  for (const ST::SystInfo& info : m_susyTools->getSystInfoList()) {
    m_systInfoMap[info.systset.name()] = info;
    systs.insert(info.systset);
    /* ATH_MSG_INFO(info); */
  }
  ATH_CHECK( m_systematicsList.addAffectingSystematics(systs) );
  ATH_CHECK( m_systematicsList.initialize());

  return StatusCode::SUCCESS;
}

StatusCode ApplySUSYToolsAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  if (m_firstEvent)
  {
    // This is a bit ugly, but we maintain our own list of systematics just to
    // make sure that we always do nominal first
    m_systs.push_back({});
    for (const CP::SystematicSet& sys : m_systematicsList.systematicsVector())
      if (!sys.name().empty())
        m_systs.push_back(sys);
    m_firstEvent = false;
  }
  
  ATH_CHECK(m_susyTools->ApplyPRWTool());

  xAOD::ElectronContainer* nominalElectrons = nullptr;
  xAOD::MuonContainer* nominalMuons = nullptr;
  xAOD::PhotonContainer* nominalPhotons = nullptr;
  xAOD::JetContainer* nominalJets = nullptr;
  xAOD::TauJetContainer* nominalTaus = nullptr;
  for (const CP::SystematicSet& sys : m_systs)
  {
    const ST::SystInfo& systInfo = m_systInfoMap[sys.name()];
    bool isNominal = sys.name().empty();
    if (!(isNominal || systInfo.affectsKinematics))
      // right now I'm only checking kinematics - weights will have to follow
      continue;
    constexpr bool RECORD_SG = false;

    const xAOD::EventInfo* evtInfo = nullptr;
    {
      const xAOD::EventInfo* inEvtInfo = nullptr;
      ATH_CHECK( evtStore()->retrieve(inEvtInfo, "EventInfo"));

      std::pair<xAOD::EventInfo*, xAOD::ShallowAuxInfo*> evtInfoSC = xAOD::shallowCopyObject(*inEvtInfo);
      std::string name = CP::makeSystematicsName(m_outputPrefix+"EventInfo_%SYS%", sys);
      ATH_CHECK( evtStore()->record(evtInfoSC.first, name));
      ATH_CHECK( evtStore()->record(evtInfoSC.second, name+"Aux."));
      evtInfo = evtInfoSC.first;
    }

    xAOD::ElectronContainer* electrons = nullptr;
    xAOD::ShallowAuxContainer* electronsAux = nullptr;
    if (isNominal || systInfo.affectsType == ST::Electron) {
      ATH_CHECK( m_susyTools->GetElectrons(electrons, electronsAux, RECORD_SG) );
    }
    else {
      std::tie(electrons, electronsAux) = xAOD::shallowCopyContainer(*nominalElectrons);
      xAOD::setOriginalObjectLink(*nominalElectrons, *electrons);
    }
    if (isNominal)
      nominalElectrons = electrons;
    std::string name = CP::makeSystematicsName(m_outputPrefix+"Electrons_%SYS%", sys);
    ATH_CHECK( evtStore()->record(electrons, name));
    ATH_CHECK( evtStore()->record(electronsAux, name+"Aux."));

    xAOD::MuonContainer* muons = nullptr;
    xAOD::ShallowAuxContainer* muonsAux = nullptr;
    if (isNominal || systInfo.affectsType == ST::Muon) {
      ATH_CHECK( m_susyTools->GetMuons(muons, muonsAux, RECORD_SG));
    }
    else {
      std::tie(muons, muonsAux) = xAOD::shallowCopyContainer(*nominalMuons);
      xAOD::setOriginalObjectLink(*nominalMuons, *muons);
    }
    if (isNominal)
      nominalMuons = muons;
    name = CP::makeSystematicsName(m_outputPrefix+"Muons_%SYS%", sys);
    ATH_CHECK( evtStore()->record(muons, name));
    ATH_CHECK( evtStore()->record(muonsAux, name+"Aux."));

    xAOD::PhotonContainer* photons = nullptr;
    xAOD::ShallowAuxContainer* photonsAux = nullptr;
    if (isNominal || systInfo.affectsType == ST::Photon) {
      ATH_CHECK( m_susyTools->GetPhotons(photons, photonsAux, RECORD_SG));
    }
    else {
      std::tie(photons, photonsAux) = xAOD::shallowCopyContainer(*nominalPhotons);
      xAOD::setOriginalObjectLink(*nominalPhotons, *photons);
    }
    if (isNominal)
      nominalPhotons = photons;
    name = CP::makeSystematicsName(m_outputPrefix+"Photons_%SYS%", sys);
    ATH_CHECK( evtStore()->record(photons, name));
    ATH_CHECK( evtStore()->record(photonsAux, name+"Aux."));


    xAOD::TauJetContainer* taus = nullptr;
    xAOD::ShallowAuxContainer* tausAux = nullptr;
    if (isNominal || systInfo.affectsType == ST::Tau) {
      ATH_CHECK( m_susyTools->GetTaus(taus, tausAux, RECORD_SG));
    }
    else {
      std::tie(taus, tausAux) = xAOD::shallowCopyContainer(*nominalTaus);
      xAOD::setOriginalObjectLink(*nominalTaus, *taus);
    }
    if (isNominal)
      nominalTaus = taus;
    name = CP::makeSystematicsName(m_outputPrefix+"Taus_%SYS%", sys);
    ATH_CHECK( evtStore()->record(taus, name));
    ATH_CHECK( evtStore()->record(tausAux, name+"Aux."));

    xAOD::JetContainer* jets = nullptr;
    xAOD::ShallowAuxContainer* jetsAux = nullptr;
    if (isNominal || systInfo.affectsType == ST::Jet) {
      if (isNominal)
        ATH_CHECK( m_susyTools->GetJets(jets, jetsAux, RECORD_SG) );
      else
        ATH_CHECK( m_susyTools->GetJetsSyst(*nominalJets, jets, jetsAux, RECORD_SG) );
    }
    else {
      std::tie(jets, jetsAux) = xAOD::shallowCopyContainer(*nominalJets);
      xAOD::setOriginalObjectLink(*nominalJets, *jets);
    }
    if (isNominal)
      nominalJets = jets;
    name = CP::makeSystematicsName(m_outputPrefix+"Jets_%SYS%", sys);
    ATH_CHECK( evtStore()->record(jets, name));
    ATH_CHECK( evtStore()->record(jetsAux, name+"Aux."));

    ATH_CHECK( m_susyTools->OverlapRemoval(electrons, muons, jets, photons, taus) );

    std::unique_ptr<xAOD::MissingETContainer> met = std::make_unique<xAOD::MissingETContainer>();
    std::unique_ptr<xAOD::MissingETAuxContainer> metAux = std::make_unique<xAOD::MissingETAuxContainer>();
    met->setStore(metAux.get());
    ATH_CHECK( m_susyTools->GetMET(*met, jets, electrons, muons, photons, taus));
  }

  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}
