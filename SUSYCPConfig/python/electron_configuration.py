from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import (
    makeElectronAnalysisSequence,
)
from AnaAlgorithm.DualUseConfig import addPrivateTool, createAlgorithm


def baseline_electron_sequence(data_type, configuration, postfix="baseline"):
    # extract information from the configuration
    conf = configuration.group("EleBaseline")
    iso = "NonIso" if conf.Iso == "None" else conf.Iso
    # If a custom configuration file is requested, then we must be calculating
    # the likelihood
    recomputeLikelihood = conf.Config != "None"
    wp = "LooseBL" if conf.Id == "LooseAndBLayerLLH" else conf.Id

    sequence = makeElectronAnalysisSequence(
        data_type,
        workingPoint="{}.{}".format(wp, iso),
        ptSelectionOutput=True,
        postfix=postfix,
        shallowViewOutput=False,
        deepCopyOutput=False,
        recomputeLikelihood=recomputeLikelihood,
        crackVeto=conf.CrackVeto,
    )
    if postfix:
        postfix = "_" + postfix
    # We need to hack in the eta/pt selections
    getattr(sequence, "ElectronPtCutAlg" + postfix).selectionTool.minPt = conf.Pt
    getattr(sequence, "ElectronEtaCutAlg" + postfix).selectionTool.maxEta = conf.Eta
    if conf.Config != "None":
        tool = getattr(sequence, "ElectronLikelihoodAlg" + postfix).selectionTool
        tool.ConfigFile = conf.Config
        # In order for the new config file to be used, the working point must be
        # set empty
        tool.WorkingPoint = ""

    # Also modify the track selections
    track_alg = getattr(sequence, "ElectronTrackSelectionAlg" + postfix)
    # For these, SUSYTools uses negative to mean no cut, the CP algs use 0
    track_alg.maxD0Significance = max(0, conf.d0sig)
    track_alg.maxDeltaZ0SinTheta = max(0, conf.z0)

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "ElectronSummarySelection" + postfix
    )
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "baseline,as_char"
    sequence.append(
        summary_alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        dynConfig={
            "selectionTool.selectionFlags": lambda meta: [
                name + (",as_bits" if "," not in name else "")
                for name in meta["selectionDecorNames"]
            ]
        },
    )

    return sequence


def signal_electron_sequence(data_type, configuration, postfix="signal"):
    conf = configuration.group("Ele")

    iso = "NonIso" if conf.Iso == "None" else conf.Iso
    # If a custom configuration file is requested, then we must be calculating
    # the likelihood
    recomputeLikelihood = conf.Config != "None"
    wp = "LooseBLLH" if conf.Id == "LooseAndBLayerLLH" else conf.Id

    sequence = makeElectronAnalysisSequence(
        data_type,
        workingPoint="{}.{}".format(wp, iso),
        postfix=postfix,
        recomputeLikelihood=recomputeLikelihood,
        shallowViewOutput=False,
        deepCopyOutput=False,
        crackVeto=conf.CrackVeto,
    )
    # Remove the calibration stage
    sequence.removeStage("calibration")
    postfix = "_" + postfix

    # We need to hack in the eta/pt selections
    getattr(sequence, "ElectronPtCutAlg" + postfix).selectionTool.minPt = conf.Et
    getattr(sequence, "ElectronPtCutAlg" + postfix).selectionTool.maxEta = conf.Eta
    if conf.Config != "None":
        tool = getattr(sequence, "ElectronLikelihoodAlg" + postfix).selectionTool
        tool.ConfigFile = conf.Config
        # In order for the new config file to be used, the working point must be
        # set empty
        tool.WorkingPoint = ""

    # Also modify the track selections
    track_alg = getattr(sequence, "ElectronTrackSelectionAlg" + postfix)
    # For these, SUSYTools uses negative to mean no cut, the CP algs use 0
    track_alg.maxD0Significance = max(0, conf.d0sig)
    track_alg.maxDeltaZ0SinTheta = max(0, conf.z0)

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "ElectronSummarySelection" + postfix
    )
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "signal,as_char"
    sequence.append(
        summary_alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        dynConfig={
            "selectionTool.selectionFlags": lambda meta: [
                name + (",as_bits" if "," not in name else "")
                for name in meta["selectionDecorNames"]
            ]
        },
    )

    return sequence
