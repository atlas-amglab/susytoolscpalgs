""" Wrap the SUSYTools configuration in a class """

from future.utils import PY3, itervalues, iteritems, string_types, iterkeys
from collections import defaultdict
if PY3:
    from collections.abc import MutableMapping, Mapping
else:
    from collections import MutableMapping, Mapping
import logging
log = logging.getLogger(__name__)


def _join(sep, *args):
    """ Join args on the string 'sep', excluding any that are None """
    return sep.join(map(str, (a for a in args if a is not None)))


class UnsupportedConfig(Exception):
    """ Exception raised if an unsupported configuration is set """

    def __init__(self, name):
        self.config_name = name
        super(UnsupportedConfig, self).__init__(
            "Configuration '{}' is not supported!".format(name)
        )


class ConfigGroup(MutableMapping):
    """ A group of configuration options

    Allows grouping configurations by a common prefix. Configuration groups can
    be nested.

    The group itself acts like a dictionary whose keys are the allowed configurations
    provided with the add_config function. Attempting to set or access a key that was not
    provided like this results in a KeyError.

    When accessing a key, if a value has been set then it is returned, otherwise the default
    is returned. Group access is recursive, so the key 'Pt' in the subgroup 'Ele' can be
    accessed by 'group["Ele.Pt"]'. Configurations can also be accessed via attribute access
    so the same could be achieved by 'group.Ele.Pt'

    Note that iterating through the configuration will iterate through all individual
     configurations (so groups will be recursed into)
    """

    def __init__(self, fail_on_unsupported=True, parent=None, name=None):
        """ Create the configuration group

        ---------
        Arguments
        ---------
        fail_on_unsupported:
            If True, raise an exception when setting an unsupported configuration value, otherwise
        parent:
            If this is a subgroup, provide its parent here
        name:
            If this is a subgroup, provide its name in the parent here
        """
        self._fail_on_unsupported = fail_on_unsupported
        self._groups = {}
        self._defaults = {}
        self._values = {}
        self._unsupported_keys = set()
        self._parent = parent
        self._name = name
        # For debugging this configuration, allow checking which values have actually been read
        # Sadly, iterating will mark *everything* as accessed...
        self._accessed = set()

    @property
    def parent(self):
        """ If this is a subgroup, the parent group that holds it """
        return self._parent

    @property
    def name(self):
        """ If this is a subgroup, then get the name of this inside the parent group """
        return self._name

    @property
    def full_name(self):
        """ The full name of this group, recursing down subgroups

        For the root group (i.e. the one with no parent) this returns 'None'
        """
        return _join('.', self.parent.name, self.name)

    @property
    def fail_on_unsupported(self):
        """ Whether to fail on accessing an unsupported key """
        return self._fail_on_unsupported

    @fail_on_unsupported.setter
    def fail_on_unsupported(self, value):
        self._fail_on_unsupported = value
        for group in itervalues(self._groups):
            group.fail_on_unsupported = value

    def add_config(self, name, default, supported=True):
        """ Add a new configuration, along with a default

        If the name includes '.' a new group is automatically created for it

        It's also possible to specify that a particular configuration is defined, but not
        supported. Depending on the value of 'fail_on_unsupported', setting an unsupported
        configuration will result in an exception.
        """
        # TEnv only supports string, int and float values
        if not isinstance(default, string_types + (int, float, bool)):
            raise TypeError("Type {} is not supported!".format(type(default)))
        if not name:
            raise ValueError("Unnamed configurations are not allowed!")
        if '.' in name:
            group, _, name = name.partition(".")
            self.group(group, create=True).add_config(
                name, default, supported)
        elif name in self._defaults:
            raise KeyError(
                "Configuration {} already exists with default value {}".format(
                    _join('.', self.full_name, name)
                ))
        else:
            self._defaults[name] = default
            if not supported:
                self._unsupported_keys.add(name)

    def group(self, name, create=False):
        """ Return a named group

        ---------
        Arguments
        ---------
        name: The name of the group
        create: If True, create the group if it's missing
        """
        # Allow for nested subgroups
        name, _, remaining = name.partition(".")
        try:
            return self._groups[name]
        except KeyError as e:
            if create:
                self._groups[name] = ConfigGroup(
                    self.fail_on_unsupported, parent=self, name=name)
                return self._groups[name]
            else:
                raise "No group '{}'".format(_join('.', self.full_name, name))

    def groups(self):
        """ Return a temprorary copy of the groups dictionary

        Modifications made to this are *not* reflected in this object!
        """
        return {k: v for k, v in iteritems(self._groups)}

    def add_group(self, name, group):
        """ Add a group to this

        The group will be made to be a child of this one
        """
        if "." in name:
            name, _, remaining = name.partition(".")
            self.group(name, create=True).add_group(remaining, group)
        elif name in self._groups:
            raise KeyError("Group '{}' already exists!".format(
                _join(".", self.full_name, name)))
        else:
            self._groups[name] = group
            group._name = name
            group._parent = parent

    def read_tenv(self, env):
        """ Read configuration values from a TEnv """
        for record in env.GetTable():
            name = record.GetName()
            # bool is handled in a special way (thanks TEnv...)
            if self.typeof(name) is bool:
                self[name] = bool(env.GetValue(name, self.default_value(name)))
            elif isinstance(self.default_value(name), string_types):
                # Have to manually remove comments for strings
                val = env.GetValue(name, self.default_value(name))
                val = val.partition("#")[0].strip()
                self[name] = val
            else:
                self[name] = env.GetValue(name, self.default_value(name))

    def from_dict(self, the_dict):
        """ Read a configuration from a dictionary """
        for k, v in iteritems(the_dict):
            if k in self._groups:
                self._groups[k].from_dict(v)
            else:
                self[k] = v

    def write_tenv(self):
        """ Create a ROOT TEnv object """
        import ROOT
        env = ROOT.TEnv()
        for k, v in iteritems(self):
            env.SetValue(k, v)
        return env

    def to_dict(self):
        """ Create a dictionary from this """
        the_dict = {k: self[k] for k in self._defaults}
        the_dict.update({k: g.to_dict() for k, g in iteritems(self._groups)})
        return the_dict

    def default_value(self, key):
        """ Get the default value of a configurable """
        if "." in key:
            group, _, key = key.partition(".")
            return self.group(group).default_value(key)
        else:
            return self._defaults[key]

    def typeof(self, key):
        """ Get the type of the configurable set by key """
        return type(self.default_value(key))

    def __contains__(self, key):
        if "." in key:
            group, _, key = key.partition(".")
            return key in self.group(group)
        else:
            return key in self._defaults or key in self._groups

    def __getitem__(self, key):
        """ Get a configured value

        Can also access groups in this way
        """
        if '.' in key:
            group, _, key = key.partition(".")
            return self.group(group)[key]
        elif key in self._groups:
            return self.group(key)
        else:
            if key in self._unsupported_keys:
                if self.fail_on_unsupported:
                    raise UnsupportedConfig(_join('.', self.full_name, key))
                else:
                    log.warning("'{}' is not supported".format(
                        _join('.', self.full_name, key)))
            try:
                val = self._values[key]
            except KeyError:
                try:
                    val = self._defaults[key]
                except KeyError:
                    raise KeyError(_join('.', self.full_name, key))
            self._accessed.add(key)
            return val

    def __setitem__(self, key, value):
        """ Set a configured value

        The type set must be the same as the default
        """
        if "." in key:
            group, _, key = key.partition(".")
            self.group(group)[key] = value
            return
        try:
            default = self._defaults[key]
        except KeyError:
            raise KeyError(_join(".", self.full_name, key))
        if type(value) is not type(default):
            raise TypeError("Value for key '{}' is of type {} but the default is of type {}".format(
                _join(".", self.full_name, key, type(value), type(default))))
        self._values[key] = value

    def __delitem__(self, key):
        """ Remove a value, will also remove the configuration """
        if key in self._groups:
            del self._groups[key]
            return
        del self._defaults[key]
        try:
            del self._values[key]
        except KeyError:
            pass
        self._unsupported_keys.remove(key)

    def __iter__(self):
        """ Iterate over all available configurations, as well as those inside groups

        Does not iterate over the groups themselves
        """
        for group in itervalues(self._groups):
            for key in group:
                yield key
        for key in iterkeys(self._defaults):
            yield key

    def __len__(self):
        return len(self._defaults) + sum(map(len, itervalues(self._groups)))

    def __getattr__(self, key):
        if not key.startswith("_") and key in self:
            return self[key]
        raise AttributeError(key)

    def __setattr__(self, key, value):
        if not key.startswith("_") and key in self:
            self[key] = value
        super(ConfigGroup, self).__setattr__(key, value)


class SUSYToolsConfig(ConfigGroup):
    """ Represent the full SUSY configuration """

    def __init__(self, fail_on_unsupported=False):
        super(SUSYToolsConfig, self).__init__(
            fail_on_unsupported=fail_on_unsupported)

        baseline_ele = self.group("EleBaseline", create=True)
        baseline_ele.add_config("Pt", 10000.)
        baseline_ele.add_config("Eta", 2.47)
        baseline_ele.add_config("Id", "LooseAndBLayerLLH")
        baseline_ele.add_config("Iso", "None")
        baseline_ele.add_config("Config", "None")
        baseline_ele.add_config("CrackVeto", False)
        baseline_ele.add_config("d0sig", -99.)
        baseline_ele.add_config("z0", 0.5)

        ele = self.group("Ele", create=True)
        ele.add_config("Et", 25000.)
        ele.add_config("Eta", 2.47)
        ele.add_config("CrackVeto", False)
        ele.add_config("Iso", "FCLoose")
        ele.add_config("IsoHighPt", "FCHighPtCaloOnly", supported=False)
        ele.add_config("IsoHighPtThresh", 200e3, supported=False)
        ele.add_config("CFT", "None", supported=False)
        ele.add_config("CFTIso", True, supported=False)
        ele.add_config("CFTSignal", True, supported=False)
        ele.add_config("DoModifiedId", False, supported=False)
        ele.add_config("Id", "TightLLH")
        ele.add_config("Config", "None")
        ele.add_config("d0sig", 5.)
        ele.add_config("z0", 0.5)
        ele.add_config("IdExpert", False)
        ele.add_config("EffNPcorrModel", "TOTAL")
        ele.add_config("EffCorrFNList", "None")
        ele.add_config("TriggerSFStringSingle",
                       "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0")
        ele.add_config(
            "EffMapFilePath", "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt")

        baseline_muon = self.group("MuonBaseline", create=True)
        baseline_muon.add_config("Pt", 10000.)
        baseline_muon.add_config("Eta", 2.7)
        baseline_muon.add_config("Iso", "None")
        baseline_muon.add_config("Id", 1)
        baseline_muon.add_config("d0sig", -99.)
        baseline_muon.add_config("z0", 0.5)

        muon = self.group("Muon", create=True)
        muon.add_config("Pt", 25000.)
        muon.add_config("Eta", 2.7)
        muon.add_config("Iso", "Loose_VarRad")
        muon.add_config("Id", 1)
        muon.add_config("IsoHighPt", "Loose_VarRad")
        muon.add_config("IsoHighPtThresh", 200e3)
        muon.add_config("d0sig", 3.)
        muon.add_config("z0", 0.5)
        muon.add_config("passedHighPt", False)
        muon.add_config("CalibrationMode", 1)

        self.add_config("MuonCosmic.z0", 1.)
        self.add_config("MuonCosmic.d0", 0.2)

        self.add_config("BadMuon.qoverp", 0.4)

        baseline_photon = self.group("PhotonBaseline", create=True)
        baseline_photon.add_config("Pt", 25000.)
        baseline_photon.add_config("Eta", 2.37)
        baseline_photon.add_config("Id", "Tight")
        baseline_photon.add_config("CrackVeto", True)
        baseline_photon.add_config("Iso", "None")

        photon = self.group("Photon", create=True)
        photon.add_config("Pt", 130000.)
        photon.add_config("Eta", 2.37)
        photon.add_config("Id", "Tight")
        photon.add_config("Iso", "FixedCutTight")
        photon.add_config(
            "TriggerName", "DI_PH_2015_2016_g20_tight_2016_g22_tight_2017_2018_g20_tight_icalovloose_L1EM15VHI")
        photon.add_config("CrackVeto", True)
        photon.add_config("AllowLate", False)

        baseline_tau = self.group("TauBaseline", create=True)
        baseline_tau.add_config("Id", "Medium")
        baseline_tau.add_config("ConfigPath", "default")

        tau = self.group("Tau", create=True)
        tau.add_config("PrePtCut", 0.)
        tau.add_config("Pt", 20000.)
        tau.add_config("Eta", 2.5)
        tau.add_config("Id", "Medium")
        tau.add_config("ConfigPath", "default")
        tau.add_config("DoTruthMatching", False)

        jet = self.group("Jet", create=True)
        jet.add_config("InputType", 9)
        jet.add_config("Pt", 20000.)
        jet.add_config("Eta", 2.8)
        jet.add_config("JvtWP", "Default")
        jet.add_config("JvtPtMax", 60.0e3)
        jet.add_config("JvtConfig", "Moriond2018/")
        jet.add_config(
            "UncertConfig", "rel21/Summer2019/R4_SR_Scenario1_SimpleJER.config")
        jet.add_config("UncertCalibArea", "default")
        jet.add_config("UncertPDsmearing", False)
        jet.add_config("LargeRcollection",
                       "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets")
        jet.add_config("LargeRuncConfig",
                       "rel21/Spring2019/R10_GlobalReduction.config")
        jet.add_config("LargRuncVars", "default")
        jet.add_config("TCCcollection",
                       "AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets")
        jet.add_config(
            "TCCuncConfig", "rel21/Summer2019/R10_Scale_TCC_all.config")
        jet.add_config(
            "WtaggerConfig", "SmoothedContainedWTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat")
        jet.add_config(
            "ZtaggerConfig", "SmoothedContainedZTagger_AntiKt10TrackCaloClusterTrimmed_MaxSignificance_3Var_MC16d_20190410.dat")
        jet.add_config("WZTaggerCalibArea", "SmoothedWZTaggers/Rel21/")
        jet.add_config(
            "ToptaggerConfig", "JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat")
        jet.add_config("TopTaggerCalibArea", "JSSWTopTaggerDNN/Rel21/")
        jet.add_config(
            "JESConfig", "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config")
        jet.add_config(
            "JESConfigAFII", "JES_MC16Recommendation_AFII_EMTopo_Apr2019_Rel21.config")
        jet.add_config(
            "JESConfigJMS", "JES_JMS_MC16Recommendation_Consolidated_MC_only_EMTopo_July2019_Rel21.config")
        jet.add_config(
            "JESConfigJMSData", "JES_JMS_MC16Recommendation_Consolidated_data_only_EMTopo_Sep2019_Rel21.config")
        jet.add_config(
            "JESConfigFat", "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config")
        jet.add_config(
            "JESConfigFatData", "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config")
        jet.add_config("CalibSeq", "JetArea_Residual_EtaJES_GSC_Insitu")
        jet.add_config("CalibSeqJMS", "JetArea_Residual_EtaJES_GSC")
        jet.add_config("CalibSeqFat", "EtaJES_JMS")
        jet.add_config("JMSCalib", False)

        track_jet = self.group("TrackJet", create=True)
        track_jet.add_config("Coll", "AntiKtVR30Rmax4Rmin02TrackJets")
        track_jet.add_config("Pt", 20000.)
        track_jet.add_config("Eta", 2.8)

        self.add_config("BadJet.Cut", "LooseBad")

        fwd_jet = self.group("FwdJet", create=True)
        fwd_jet.add_config("doJVT", False)
        fwd_jet.add_config("JvtWP", "Tight")
        fwd_jet.add_config("JvtPtMax", 120e3)
        fwd_jet.add_config("JvtEtaMin", 2.5)
        fwd_jet.add_config("JvtConfig", "May2020/")

        btag = self.group("Btag", create=True)
        btag.add_config("enable", True)
        btag.add_config("Tagger", "DL1")
        btag.add_config("WP", "FixedCutBEff_77")
        btag.add_config("MinPt", -1)
        btag.add_config("TimeStamp", "201810")
        btag.add_config(
            "CalibPath", "xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root")
        btag.add_config("SystStrategy", "Envelope")

        btag_trkjet = self.group("BtagTrkJet", create=True)
        btag_trkjet.add_config("enable", True)
        btag_trkjet.add_config("Tagger", "MV2c10")
        btag_trkjet.add_config("WP", "FixedCutBEff_77")
        btag_trkjet.add_config("MinPt", -1)
        btag_trkjet.add_config("TimeStamp", "None")
        btag_trkjet.add_config("KeyOverride", "")

        trig = self.group("Trig", create=True)
        trig.add_config(
            "Singlelep2015", "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50")
        trig.add_config(
            "Singlelep2016", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50")
        trig.add_config(
            "Singlelep2017", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50")
        trig.add_config(
            "Singlelep2018", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50")
        trig.add_config(
            "Dilep2015", "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50 || 2e12_lhloose_L12EM10VH || e17_lhloose_mu14 || e7_lhmedium_mu24 || mu18_mu8noL1 || 2mu10")
        trig.add_config("Dilep2016", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e17_lhvloose_nod0 || e17_lhloose_nod0_mu14 || e7_lhmedium_nod0_mu24 || e26_lhmedium_nod0_L1EM22VHI_mu8noL1 || mu22_mu8noL1 || 2mu14")
        trig.add_config("Dilep2017", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || e17_lhloose_nod0_mu14 || e7_lhmedium_nod0_mu24 || e26_lhmedium_nod0_mu8noL1 || mu22_mu8noL1 || 2mu14")
        trig.add_config("Dilep2018", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || e17_lhloose_nod0_mu14 || e7_lhmedium_nod0_mu24 || e26_lhmedium_nod0_mu8noL1 || mu22_mu8noL1 || 2mu14")
        trig.add_config("Multi2015", "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose || mu20_iloose_L1MU15_OR_mu50 || 2e12_lhloose_L12EM10VH || e17_lhloose_2e9_lhloose || 2e12_lhloose_mu10 || e12_lhloose_2mu10 || e17_lhloose_mu14 || e7_lhmedium_mu24 || mu18_mu8noL1 || 2mu10 || 3mu6")
        trig.add_config("Multi2016", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e17_lhvloose_nod0 || e17_lhloose_nod0_mu14 || e7_lhmedium_nod0_mu24 || e26_lhmedium_nod0_L1EM22VHI_mu8noL1 || e17_lhloose_nod0_2e9_lhloose_nod0 || e12_lhloose_nod0_2mu10 || 2e12_lhloose_nod0_mu10 || mu22_mu8noL1 || 2mu14 || 3mu6")
        trig.add_config("Multi2017", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || e17_lhloose_nod0_mu14 || e7_lhmedium_nod0_mu24 || e26_lhmedium_nod0_mu8noL1 || e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH || e12_lhloose_nod0_2mu10 || 2e12_lhloose_nod0_mu10 || mu22_mu8noL1 || 2mu14 || 3mu6")
        trig.add_config("Multi2018", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 || mu26_ivarmedium_OR_mu50 || 2e24_lhvloose_nod0 || e17_lhloose_nod0_mu14 || e7_lhmedium_nod0_mu24 || e26_lhmedium_nod0_mu8noL1 || e24_lhvloose_nod0_2e12_lhvloose_nod0_L1EM20VH_3EM10VH || e12_lhloose_nod0_2mu10 || 2e12_lhloose_nod0_mu10 || mu22_mu8noL1 || 2mu14 || 3mu6")
        trig.add_config("Diphoton2015", "g35_loose_g25_loose")
        trig.add_config("Diphotonp2016", "g35_loose_g25_loose")
        trig.add_config("Diphotonp2017", "g35_medium_g25_medium_L12EM20VH")
        trig.add_config("Diphotonp2018", "g35_medium_g25_medium_L12EM20VH")

        overlap_removal = self.group("OR", create=True)
        overlap_removal.add_config("DoBoostedElectron", True)
        overlap_removal.add_config("BoostedElectronC1", -999.)
        overlap_removal.add_config("BoostedElectronC2", -999.)
        overlap_removal.add_config("BoostedElectronMaxConeSize", -999.)
        overlap_removal.add_config("DoBoostedMuon", True)
        overlap_removal.add_config("BoostedMuonC1", -999.)
        overlap_removal.add_config("BoostedMuonC2", -999.)
        overlap_removal.add_config("BoostedMuonMaxConeSize", -999.)
        overlap_removal.add_config("DoMuonJetGhostAssociation", True)
        overlap_removal.add_config("DoTau", False)
        overlap_removal.add_config("DoPhoton", False)
        overlap_removal.add_config("EleJet", True)
        overlap_removal.add_config("ElEl", False)
        overlap_removal.add_config("ElMu", False)
        overlap_removal.add_config("MuonJet", True)
        overlap_removal.add_config("Bjet", False)
        overlap_removal.add_config("ElBjet", False)
        overlap_removal.add_config("MuBjet", False)
        overlap_removal.add_config("TauBjet", False)
        overlap_removal.add_config("MuJetApplyRelPt", False)
        overlap_removal.add_config("MuJetPtRatio", -999.)
        overlap_removal.add_config("MuJetTrkPtRatio", -999.)
        overlap_removal.add_config("RemoveCaloMuons", True)
        overlap_removal.add_config("MuJetInnerDR", -999.)
        overlap_removal.add_config("BtagWP", "FixedCutBEff_85")
        overlap_removal.add_config("InputLabel", "selected")
        overlap_removal.add_config("PhotonFavoured", False)
        overlap_removal.add_config("BJetPtUpperThres", -1.)
        overlap_removal.add_config("LinkOverlapObjects", False)

        overlap_removal.add_config("DoFatJets", False)
        overlap_removal.add_config("EleFatJetDR", -999.)
        overlap_removal.add_config("JetFatJetDR", -999.)

        self.add_config("Truth.UseTRUTH3", True)

        self.add_config("Trigger.UpstreamMatching", False)
        self.add_config("Trigger.MatchingPrefix", "")

        self.add_config("SigLep.RequireIso", True)
        self.add_config("SigEl.RequireIso", True)
        self.add_config("SigMu.RequireIso", True)
        self.add_config("SigPh.RequireIso", True)
        self.add_config("SigLepPh.UseSigLepForIsoCloseByOR", False)
        self.add_config("SigLepPh.IsoCloseByORpassLabel", "None")

        met = self.group("MET", create=True)
        met.add_config("EleTerm", "RefEle")
        met.add_config("GammaTerm", "RefGamma")
        met.add_config("TauTerm", "RefTau")
        met.add_config("JetTerm", "RefJet")
        met.add_config("MuonTerm", "Muons")
        met.add_config("InputSuffix", "")
        met.add_config("OutputTerm", "Final")
        met.add_config("RemoveOverlappingCaloTaggedMuons", True)
        self.add_config("Met.DoSetMuonJetEMScale", True)
        met.add_config("DoRemoveMuonJets", True)
        met.add_config("UseGhostMuons", False)
        met.add_config("DoMuonEloss", False)
        met.add_config("DoGreedyPhotons", False)
        met.add_config("DoVeryGreedyPhotons", False)

        met.add_config("DoTrkSyst", True)
        met.add_config("DoCaloSyst", False)
        met.add_config("DoTrkJetSyst", False)
        self.add_config("METSys.ConfigPrefix",
                        "METUtilities/data17_13TeV/prerec_Jan16")
        met.add_config("JetSelection", "Tight")
        met.add_config("SoftTermParam", 0)
        met.add_config("TreatPUJets", True)
        met.add_config("DoPhiReso", False)

        prw = self.group("PRW", create=True)
        prw.add_config(
            "ActualMu2017File", "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root")
        prw.add_config("ActualMu2018File",
                       "GoodRunsLists/data18_13TeV/20190219/purw.actualMu.root")
        prw.add_config("DataSF", 1./1.03)
        prw.add_config("DataSF_UP", 1./0.99)
        prw.add_config("DataSF_DW", 1./1.07)
        prw.add_config("UseRunDependentPrescaleWeight", False)
        prw.add_config("autoconfigPRWPath", "dev/PileupReweighting/share/")
        prw.add_config("autoconfigPRWFile", "None")
        prw.add_config("autoconfigPRWRPVmode", False)
        prw.add_config("autoconfigPRWHFFilter", "None")
        prw.add_config("autoconfigPRWRtags", "mc16a:r9364_r11505_r11285,mc16c:r9781,mc16d:r10201_r11506_r11279,mc16e:r10724_r11507_r11249,mc16ans:r10740_r10832_r10847_r11008_r11036,mc16dns:r10739_r10833_r10848_r11009_r11037,mc16ens:r10790_r11038_r11265")

        self.add_config("StrictConfigCheck", False)
