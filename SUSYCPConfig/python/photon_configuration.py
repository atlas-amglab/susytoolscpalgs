from EgammaAnalysisAlgorithms.PhotonAnalysisSequence import makePhotonAnalysisSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool


def baseline_photon_sequence(data_type, configuration, postfix="baseline"):
    # extract information from the configuration
    conf = configuration.group("PhotonBaseline")

    sequence = makePhotonAnalysisSequence(
        data_type,
        workingPoint="{}.{}".format(conf.Id, conf.Iso),
        deepCopyOutput=False,
        postfix=postfix
    )

    if postfix:
        postfix = "_" + postfix

    delattr(sequence, "PhotonViewFromSelectionAlg"+postfix)
    # Shouldn't apply the shower shape fudge alg on derivations
    delattr(sequence, "PhotonShowerShapeFudgeAlg"+postfix)

    if conf.Iso == "None":
        # Remove the isolation algorithm
        delattr(sequence, "PhotonIsolationCorrectionAlg"+postfix)
        delattr(sequence, "PhotonIsolationSelectionAlg"+postfix)

    kin_alg = createAlgorithm("CP::AsgSelectionAlg",
                              "PhotonPtEtaCutAlg" + postfix)
    kin_alg.selectionDecoration = "selectPhotonPtEta" + postfix + ",as_char"
    addPrivateTool(kin_alg, "selectionTool", "CP::AsgPtEtaSelectionTool")
    kin_alg.selectionTool.maxEta = conf.Eta
    kin_alg.selectionTool.minPt = conf.Pt
    if conf.CrackVeto:
        kin_alg.selectionTool.etaGapLow = 1.37
        kin_alg.selectionTool.etaGapHigh = 1.52
    kin_alg.selectionTool.useClusterEta = True
    sequence.append(
        kin_alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        stageName="selection",
        metaConfig={
            'selectionDecorNames': [kin_alg.selectionDecoration],
            'selectionDecorCount': [1]},
        dynConfig={'preselection': lambda meta: "&&".join(
            meta["selectionDecorNames"])}
    )

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "PhotonSummarySelection"+postfix)
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "baseline,as_char"
    sequence.append(summary_alg,
                    inputPropName="particles",
                    outputPropName="particlesOut",
                    dynConfig={
                        "selectionTool.selectionFlags": lambda meta:
                            [name + (",as_bits" if "," not in name else "")
                             for name in meta["selectionDecorNames"]]
                    }
                    )

    return sequence


def signal_photon_sequence(data_type, configuration, postfix="signal"):
    # extract information from the configuration
    conf = configuration.group("Photon")

    sequence = makePhotonAnalysisSequence(
        data_type,
        workingPoint="{}.{}".format(conf.Id, conf.Iso),
        deepCopyOutput=False,
        postfix=postfix
    )
    sequence.removeStage("calibration")

    if postfix:
        postfix = "_" + postfix
    delattr(sequence, "PhotonViewFromSelectionAlg"+postfix)

    if conf.Iso == "None":
        # Remove the isolation algorithm
        delattr(sequence, "PhotonIsolationCorrectionAlg"+postfix)
        delattr(sequence, "PhotonIsolationSelectionAlg"+postfix)

    kin_alg = createAlgorithm("CP::AsgSelectionAlg",
                              "PhotonPtEtaCutAlg" + postfix)
    kin_alg.selectionDecoration = "selectPtEta" + postfix + ",as_char"
    addPrivateTool(kin_alg, "selectionTool", "CP::AsgPtEtaSelectionTool")
    kin_alg.selectionTool.maxEta = conf.Eta
    kin_alg.selectionTool.minPt = conf.Pt
    if conf.CrackVeto:
        kin_alg.selectionTool.etaGapLow = 1.37
        kin_alg.selectionTool.etaGapHigh = 1.52
    kin_alg.selectionTool.useClusterEta = True
    sequence.append(
        kin_alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        stageName="selection",
        metaConfig={
            'selectionDecorNames': [kin_alg.selectionDecoration],
            'selectionDecorCount': [1]},
        dynConfig={'preselection': lambda meta: "&&".join(
            meta["selectionDecorNames"])}
    )

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "PhotonSummarySelection"+postfix)
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "signal,as_char"
    sequence.append(summary_alg,
                    inputPropName="particles",
                    outputPropName="particlesOut",
                    dynConfig={
                        "selectionTool.selectionFlags": lambda meta:
                            [name + (",as_bits" if "," not in name else "")
                             for name in meta["selectionDecorNames"]]
                    }
                    )

    return sequence
