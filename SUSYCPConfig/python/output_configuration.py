from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.AlgSequence import AlgSequence


def output_sequence(tree_name, container_prefix, configuration, stream_name):
    """ Handle the bulk of what we want to output """
    algSeq = AlgSequence(tree_name + "_seq")
    treeMaker = createAlgorithm("CP::TreeMakerAlg", tree_name + "_maker")
    treeMaker.TreeName = tree_name
    treeMaker.RootStreamName = stream_name
    algSeq += treeMaker

    ntupleMaker = createAlgorithm("CP::AsgxAODNTupleMakerAlg", tree_name + "_branches")
    ntupleMaker.TreeName = tree_name
    ntupleMaker.RootStreamName = stream_name
    ntupleMaker.systematicsRegex = ".*"
    branches = []
    for cont_name in ("Electrons", "Muons", "Photons", "Jets", "Taus"):
        branches += [
            "{0}{1}_%SYS%.{2} -> {1}_%SYS%_{2}".format(
                container_prefix, cont_name, value
            )
            # for value in ("pt", "eta", "phi", "baseline", "passOR")
            for value in ("pt", "eta", "phi")
        ]
        if cont_name is not "Jets":
            branches += [
                "{0}{1}_%SYS%.signal -> {1}_%SYS%_signal".format(
                    container_prefix, cont_name
                )
            ]
    ntupleMaker.Branches = branches
    algSeq += ntupleMaker

    treeFiller = createAlgorithm("CP::TreeFillerAlg", tree_name + "_filler")
    treeFiller.TreeName = tree_name
    treeFiller.RootStreamName = stream_name
    algSeq += treeFiller
    return algSeq
