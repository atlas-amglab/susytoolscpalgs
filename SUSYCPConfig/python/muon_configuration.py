from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
import ROOT
from future.utils import iteritems


def baseline_muon_sequence(data_type, configuration, postfix="baseline"):
    conf = configuration.group("MuonBaseline")
    # For ID working points, need to convert from the numbers used by SUSYTools
    # to the names used by the CP algs. There are also a couple of extra properties
    # on the tool that may need to be set
    extra_sel_props = {}
    for wp in ("Tight", "Medium", "Loose", "VeryLoose"):
        if conf.Id == getattr(ROOT.xAOD.Muon, wp):
            id = wp
            break
    else:
        if conf.Id == 4:
            id = "HighPt"
        elif conf.Id == 5:
            id = "LowPtEfficiency"
        elif conf.Id == 6:
            id = "LowPtEfficiency"
            extra_sel_props["UseMVALowPt"] = True
        elif conf.Id == 7:
            id = "HighPt"
            extra_sel_props["Use2stationMuonsHighPt"] = False
        else:
            raise ValueError(
                "Invalid value {} for MuonBaseline.Id".format(conf.Id))

    sequence = makeMuonAnalysisSequence(
        data_type,
        workingPoint="{}.{}".format(
            id, "NonIso" if conf.Iso == "None" else "Iso"),
        ptSelectionOutput=True,
        shallowViewOutput=False,
        deepCopyOutput=False,
        postfix=postfix
    )

    if postfix:
        postfix = "_"+postfix

    # Set the calibration mode
    getattr(sequence, "MuonCalibrationAndSmearingAlg" +
            postfix).calibrationAndSmearingTool.calibrationMode = configuration.Muon.CalibrationMode

    # Apply the extra selection tool cuts here
    sel_tool = getattr(sequence, "MuonSelectionAlg"+postfix).selectionTool
    for k, v in iteritems(extra_sel_props):
        setattr(sel_tool, k, v)
    # Because the muon sequence doesn't actually set a proper isolation working point,
    # do that here
    if conf.Iso != "None":
        getattr(sequence, "MuonIsolationAlg" +
                postfix).isolationTool.MuonWP = conf.Iso

    # Hack pt/eta selections
    getattr(sequence, "MuonEtaCutAlg"+postfix).selectionTool.maxEta = conf.Eta
    getattr(sequence, "MuonPtCutAlg"+postfix).selectionTool.minPt = conf.Pt

    # Add the d0sig/z0 selections
    track_alg = getattr(sequence, "MuonTrackSelectionAlg"+postfix)
    # For these, SUSYTools uses negative to mean no cut, the CP algs use 0
    track_alg.maxD0Significance = max(0, conf.d0sig)
    track_alg.maxDeltaZ0SinTheta = max(0, conf.z0)

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "MuonSummarySelection"+postfix)
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "baseline,as_char"
    sequence.append(summary_alg,
                    inputPropName="particles",
                    outputPropName="particlesOut",
                    dynConfig={
                        "selectionTool.selectionFlags": lambda meta:
                            [name + (",as_bits" if "," not in name else "")
                             for name in meta["selectionDecorNames"]]
                    }
                    )

    return sequence


def signal_muon_sequence(data_type, configuration, postfix="signal"):
    conf = configuration.group("Muon")
    # For ID working points, need to convert from the numbers used by SUSYTools
    # to the names used by the CP algs. There are also a couple of extra properties
    # on the tool that may need to be set
    extra_sel_props = {}
    for wp in ("Tight", "Medium", "Loose", "VeryLoose"):
        if conf.Id == getattr(ROOT.xAOD.Muon, wp):
            id = wp
            break
    else:
        if conf.Id == 4:
            id = "HighPt"
        elif conf.Id == 5:
            id = "LowPtEfficiency"
        elif conf.Id == 6:
            id = "LowPtEfficiency"
            extra_sel_props["UseMVALowPt"] = True
        elif conf.Id == 7:
            id = "HighPt"
            extra_sel_props["Use2stationMuonsHighPt"] = False
        else:
            raise ValueError(
                "Invalid value {} for MuonBaseline.Id".format(conf.Id))

    sequence = makeMuonAnalysisSequence(
        data_type,
        workingPoint="{}.{}".format(
            id, "NonIso" if conf.Iso == "None" else "Iso"),
        shallowViewOutput=False,
        deepCopyOutput=False,
        ptSelectionOutput=True,
        postfix=postfix
    )
    # Remove the calibration stage
    sequence.removeStage("calibration")

    if postfix:
        postfix = "_"+postfix

    # Apply the extra selection tool cuts here
    sel_tool = getattr(sequence, "MuonSelectionAlg"+postfix).selectionTool
    for k, v in iteritems(extra_sel_props):
        setattr(sel_tool, k, v)
    # Because the muon sequence doesn't actually set a proper isolation working point,
    # do that here
    if conf.Iso != "None":
        getattr(sequence, "MuonIsolationAlg" +
                postfix).isolationTool.MuonWP = conf.Iso

    # Hack pt/eta selections
    getattr(sequence, "MuonEtaCutAlg"+postfix).selectionTool.maxEta = conf.Eta
    getattr(sequence, "MuonPtCutAlg"+postfix).selectionTool.minPt = conf.Pt

    # Add the d0sig/z0 selections
    track_alg = getattr(sequence, "MuonTrackSelectionAlg"+postfix)
    # For these, SUSYTools uses negative to mean no cut, the CP algs use 0
    track_alg.maxD0Significance = max(0, conf.d0sig)
    track_alg.maxDeltaZ0SinTheta = max(0, conf.z0)

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "MuonSummarySelection"+postfix)
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "signal,as_char"
    sequence.append(summary_alg,
                    inputPropName="particles",
                    outputPropName="particlesOut",
                    dynConfig={
                        "selectionTool.selectionFlags": lambda meta:
                            [name + (",as_bits" if "," not in name else "")
                             for name in meta["selectionDecorNames"]]
                    }
                    )

    return sequence
