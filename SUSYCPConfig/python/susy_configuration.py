from .susytools_config import SUSYToolsConfig
from .electron_configuration import baseline_electron_sequence, signal_electron_sequence
from .muon_configuration import baseline_muon_sequence, signal_muon_sequence
from .photon_configuration import baseline_photon_sequence, signal_photon_sequence
from .tau_configuration import baseline_tau_sequence, signal_tau_sequence
from .jet_configuration import deduce_jet_collection, r04_jet_sequence
from .overlap_configuration import overlap_sequence
from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence


def full_sequence(data_type, configuration, prwFiles=[], lumiCalcFiles=[]):
    """ Create the full SUSY sequence """
    sequence = AlgSequence()

    # Put signal selections after baseline selections so that they dont' get affected by the OR systematics

    prw_sequence = makePileupAnalysisSequence(
        data_type, userLumicalcFiles=lumiCalcFiles, userPileupConfigs=prwFiles
    )
    prw_sequence.configure(inputName="EventInfo", outputName="CPEventInfo_%SYS%")
    sequence += prw_sequence

    baseline_electrons = baseline_electron_sequence(data_type, configuration)
    baseline_electrons.configure(
        inputName="Electrons", outputName="AnalysisElectrons_%SYS%"
    )
    sequence += baseline_electrons

    baseline_muons = baseline_muon_sequence(data_type, configuration)
    baseline_muons.configure(inputName="Muons", outputName="AnalysisMuons_%SYS%")
    sequence += baseline_muons

    baseline_photons = baseline_photon_sequence(data_type, configuration)
    baseline_photons.configure(inputName="Photons", outputName="AnalysisPhotons_%SYS%")
    sequence += baseline_photons

    baseline_taus = baseline_tau_sequence(data_type, configuration)
    baseline_taus.configure(inputName="TauJets", outputName="AnalysisTaus_%SYS%")
    sequence += baseline_taus

    r04_jets = r04_jet_sequence(data_type, configuration)
    input_jets = deduce_jet_collection(configuration, btag_copy=True)
    r04_jets.configure(inputName=input_jets, outputName="AnalysisJets_%SYS%")
    sequence += r04_jets

    # Now do OR
    OR_seq = overlap_sequence(data_type, configuration)
    OR_seq.configure(
        inputName={
            "electrons": "AnalysisElectrons_%SYS%",
            "muons": "AnalysisMuons_%SYS%",
            "photons": "AnalysisPhotons_%SYS%",
            "jets": "AnalysisJets_%SYS%",
            "taus": "AnalysisTaus_%SYS%",
        },
        outputName={
            "electrons": "AnalysisElectronsOR_%SYS%",
            "muons": "AnalysisMuonsOR_%SYS%",
            "photons": "AnalysisPhotonsOR_%SYS%",
            "jets": "CPJets_%SYS%",
            "taus": "AnalysisTausOR_%SYS%",
        },
        affectingSystematics={
            "electrons": baseline_electrons.affectingSystematics(),
            "photons": baseline_photons.affectingSystematics(),
            "muons": baseline_muons.affectingSystematics(),
            "jets": r04_jets.affectingSystematics(),
            "taus": baseline_taus.affectingSystematics(),
        },
    )
    sequence += OR_seq
    view_jet = createAlgorithm("CP::AsgViewFromSelectionAlg", "CPJetsViewAlg")
    view_jet.selection = ["passOR,as_char"]
    view_jet.input = "CPJets_%SYS%"
    view_jet.output = "CPPassORJets_%SYS%"
    view_jet.inputRegex = OR_seq.affectingSystematics("jets")
    sequence += view_jet

    # Now take care of the signal sequences.
    signal_electrons = signal_electron_sequence(data_type, configuration)
    signal_electrons.configure(
        inputName="AnalysisElectronsOR_%SYS%",
        outputName="CPElectrons_%SYS%",
        affectingSystematics=OR_seq.affectingSystematics("electrons"),
    )
    sequence += signal_electrons
    view_muons = createAlgorithm("CP::AsgViewFromSelectionAlg", "CPElectronsViewAlg")
    view_muons.selection = ["passOR,as_char"]
    view_muons.input = "CPElectrons_%SYS%"
    view_muons.output = "CPPassORElectrons_%SYS%"
    view_muons.inputRegex = signal_electrons.affectingSystematics()
    sequence += view_muons


    signal_muons = signal_muon_sequence(data_type, configuration)
    signal_muons.configure(
        inputName="AnalysisMuonsOR_%SYS%",
        outputName="CPMuons_%SYS%",
        affectingSystematics=OR_seq.affectingSystematics("muons"),
    )
    sequence += signal_muons
    view_muons = createAlgorithm("CP::AsgViewFromSelectionAlg", "CPMuonsViewAlg")
    view_muons.selection = ["passOR,as_char"]
    view_muons.input = "CPMuons_%SYS%"
    view_muons.output = "CPPassORMuons_%SYS%"
    view_muons.inputRegex = signal_muons.affectingSystematics()
    sequence += view_muons

    signal_photons = signal_photon_sequence(data_type, configuration)
    signal_photons.configure(
        inputName="AnalysisPhotonsOR_%SYS%",
        outputName="CPPhotons_%SYS%",
        affectingSystematics=OR_seq.affectingSystematics("photons"),
    )
    sequence += signal_photons
    view_photons = createAlgorithm("CP::AsgViewFromSelectionAlg", "CPPhotonsViewAlg")
    view_photons.selection = ["passOR,as_char"]
    view_photons.input = "CPPhotons_%SYS%"
    view_photons.output = "CPPassORPhotons_%SYS%"
    view_photons.inputRegex = signal_photons.affectingSystematics()
    sequence += view_photons

    signal_taus = signal_tau_sequence(data_type, configuration)
    signal_taus.configure(
        inputName="AnalysisTausOR_%SYS%",
        outputName="CPTaus_%SYS%",
        affectingSystematics=OR_seq.affectingSystematics("taus"),
    )
    sequence += signal_taus
    view_taus = createAlgorithm("CP::AsgViewFromSelectionAlg", "CPTausViewAlg")
    view_taus.selection = ["passOR,as_char"]
    view_taus.input = "CPTaus_%SYS%"
    view_taus.output = "CPPassORTaus_%SYS%"
    view_taus.inputRegex = signal_taus.affectingSystematics()
    sequence += view_taus

    # This is messy as hell, but to make the filters for the non-CP sequences I
    # need to know the systematic patterns.
    #
    # I do not like this - I want to shift the systematics handling out of the
    # python as far as possible.
    syst_patterns = {
            "jets" : OR_seq.affectingSystematics("jets"),
            "electrons": signal_electrons.affectingSystematics(),
            "photons" : signal_photons.affectingSystematics(),
            "muons" : signal_muons.affectingSystematics(),
            "taus" : signal_taus.affectingSystematics()
            }

    return sequence, syst_patterns
