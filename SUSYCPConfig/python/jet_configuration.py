from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
from JetAnalysisAlgorithms.JetJvtAnalysisSequence import makeJetJvtAnalysisSequence
from FTagAnalysisAlgorithms.FTagAnalysisSequence import makeFTagAnalysisSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool


def deduce_jet_collection(conf, btag_copy=False):
    """ Work out which jet collection we're using

    If btag_copy is True then returns the full name including the btagging timestamp
    """
    # NB - only supported R=0.4 EMTopo and EMPFlow jets here
    if conf.Jet.InputType == 1:
        base = "AntiKt4EMTopoJets"
    elif conf.Jet.InputType == 9:
        base = "AntiKt4EMPFlowJets"
    else:
        raise ValueError(
            "Unsupported value for Jet.InputType: '{}'".format(conf.InputType))

    if btag_copy and conf.Btag.enable and conf.Btag.TimeStamp:
        return base + "_BTagging{}".format(conf.Btag.TimeStamp)
    else:
        return base


def r04_jet_sequence(data_type, configuration, postfix="jets"):
    conf = configuration.group("Jet")

    do_fjvt = configuration.FwdJet.doJVT
    do_jvt = conf.JvtWP != ""
    jet_coll = deduce_jet_collection(configuration)

    sequence = makeJetAnalysisSequence(
        data_type,
        jetCollection=jet_coll,
        runFJvtUpdate=do_fjvt,
        runFJvtSelection=do_fjvt,
        runJvtUpdate=do_jvt,
        runJvtSelection=do_jvt,
        postfix=postfix)

    if postfix:
        postfix = "_"+postfix

    if configuration.Btag.enable and configuration.Btag.TimeStamp != "None":
        alg = createAlgorithm(
            "CP::AsgOriginalObjectLinkAlg", "createJetOriginalObjectLinks"+postfix
        )
        alg.baseContainerName = jet_coll
        sequence.insert(0, alg,
                        inputPropName="particles",
                        outputPropName="particlesOut")

    # Figure out which of the jet calibration files we need
    if conf.JMSCalib:
        jes_config = conf.JESConfigJMSData if data_type == "data" else conf.JESConfigJMS
        calib_seq = conf.CalibSeqJMS
        if data_type == "data":
            calib_seq += "_JMS_Insitu"
        else:
            calib_seq += "_Smear_JMS"
    else:
        if data_type in ("data", "mc"):
            jes_config = conf.JESConfig
        else:
            jes_config = conf.JESConfigAFII

        calib_seq = conf.CalibSeq
        # If 'Insitu' == in the string and it's not data, remove insitu and add _Smear
        if data_type != "data":
            calib_seq = calib_seq.replace("_Insitu", "") + "_Smear"
    if conf.InputType == 9:
        jes_config = jes_config.replace("_EMTopo_", "_PFlow_")

    tool = getattr(sequence, "JetCalibrationAlg"+postfix).calibrationTool
    tool.ConfigFile = jes_config
    tool.CalibSequence = calib_seq

    tool = getattr(sequence, "JetUncertaintiesTool" +
                   postfix).uncertaintiesTool
    tool.ConfigFile = conf.UncertConfig
    if conf.UncertCalibArea != "default":
        tool.CalibArea = conf.UncertCalibArea

    if do_jvt:
        alg = getattr(sequence, "JvtEfficiencyAlg"+postfix)
        tool = alg.efficiencyTool
        tool.WorkingPoint = conf.JvtWP
        tool.MaxPtForJvt = conf.JvtPtMax
        tool.SFFile = "JetJvtEfficiency/{}/JvtSFFile_{}.root".format(
            conf.JvtConfig,
            "EMTopoJets" if conf.InputType == 1 else "EMPFlowJets"
        )

        if data_type != "data":
            # Which of these to run is hard to tell, but this is the one saved in DAOD_PHYS so let's hardcode this one...
            alg.truthJetCollection = "AntiKt4TruthDressedWZJets"

    if do_fjvt:
        tool = getattr(sequence, "JetModifierAlg"+postfix).modifierTool
        tool.UseTightOP = configuration.FwdJet.JvtWP == "Tight"
        tool.ForwardMaxPt = configuration.FwdJet.JvtPtMax
        tool.EtaThresh = configuration.FwdJet.JvtEtaMin

        tool = getattr(sequence, "ForwardJvtEfficiencyAlg").efficiencyTool
        tool.SFFile = "JetJvtEfficiency/{}/fJvtSFFile.{}.root".format(
            configuration.JvtConfig,
            "EMtopo" if conf.InputType == 1 else "EMPFlow"
        )
        tool.WorkingPoint = configuration.FwdJet.JvtWP
        tool.MaxPtForJvt = configuration.FwdJet.JvtPtMax

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "JetSummarySelection"+postfix)
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "baseline,as_char"
    sequence.append(summary_alg,
                    inputPropName="particles",
                    outputPropName="particlesOut",
                    dynConfig={
                        "selectionTool.selectionFlags": lambda meta:
                            [name + (",as_bits" if "," not in name else "")
                             for name in meta["selectionDecorNames"]]
                    }
                    )

    if configuration.Btag.enable:
        ftag_sequence(data_type, configuration, jet_coll, sequence,
                      conf_group="Btag",
                      postfix=postfix[1:] if postfix else "")

    return sequence


def ftag_sequence(data_type, configuration, jetCollection, sequence, conf_group, postfix="ftag"):
    conf = configuration.group(conf_group)

    if "BTagging" not in jetCollection and conf.TimeStamp != "None":
        jetCollection += "_BTagging{}".format(conf.TimeStamp)

    if postfix:
        postfix = "_"+postfix
    makeFTagAnalysisSequence(
        sequence, data_type, jetCollection,
        btagWP=conf.WP,
        btagger=conf.Tagger,
        minPt=conf.MinPt if conf.MinPt > 0 else None,
        postfix=postfix
    )

    tool = getattr(sequence, "FTagSelectionAlg" +
                   conf.Tagger+conf.WP+postfix).selectionTool
    tool.FlvTagCutDefinitionsFileName = configuration.Btag.CalibPath

    if data_type != "data":
        sf_tool = getattr(sequence, "FTagEfficiencyScaleFactorAlg" +
                          conf.Tagger+conf.WP+postfix).efficiencyTool
        sf_tool.ScaleFactorFileName = configuration.Btag.CalibPath
        sf_tool.SystematicsStrategy = configuration.Btag.SystStrategy

    return sequence
