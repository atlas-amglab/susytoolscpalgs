from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
from .jet_configuration import deduce_jet_collection


def overlap_sequence(data_type, configuration, postfix="OR"):
    conf = configuration.group("OR")

    bjet_label = ""
    if any((conf.Bjet, conf.ElBjet, conf.MuBjet, conf.TauBjet)):
        bjet_label = "ftag_select_" + configuration.Btag.Tagger + "_" + conf.BtagWP

    sequence = makeOverlapAnalysisSequence(
        dataType=data_type,
        inputLabel="baseline",
        outputLabel="passOR",
        boostedLeptons=conf.DoBoostedElectron or conf.doBoostedMuon,
        doTaus=conf.DoTau,
        doPhotons=conf.DoPhoton,
        doFatJets=conf.DoFatJets,
        doEleEleOR=conf.ElEl,
        linkOverlapObjects=conf.LinkOverlapObjects,
        doMuPFJetOR=configuration.Jet.InputType == 9,
        postfix=postfix,
        bJetLabel=bjet_label,
    )

    if any((conf.Bjet, conf.ElBjet, conf.MuBjet, conf.TauBjet)) and \
            conf.BtagWP != configuration.Btag.WP:
        # We need to sequence an extra b-tagging tool
        alg = createAlgorithm("CP::AsgSelectionAlg", "FTagSelectionAlgOR" +
                              configuration.Btag.Tagger+conf.BtagWP+postfix)
        addPrivateTool(alg, "selectionTool", "BTaggingSelectionTool")
        alg.selectionTool.TaggerName = configuration.Btag.Tagger
        alg.selectionTool.OperatingPoint = conf.BtagWP
        alg.selectionTool.JetAuthor = deduce_jet_collection(
            configuration, btag_copy=True)
        alg.selectionTool.FlvTagCutDefinitionsFilename = configuration.Btag.CalibPath
        alg.selectionTool.MinPt = configuration.Btag.MinPt
        alg.selectionDecoration = bjet_label
        sequence.insert(
            0,
            alg,
            inputPropName={"jets": "particles"},
            outputPropName={"jets": "particlesOut"}
        )

    or_tool = getattr(sequence, "OverlapRemovalAlg"+postfix).overlapTool

    if conf.DoTau and conf.TauBjet:
        or_tool.TauJetORT.BJetLabel = bjet_label

    or_tool.EleJetORT.UseSlidingDR = conf.DoBoostedElectron
    if conf.DoBoostedElectron:
        if conf.BoostedElectronC1 > 0:
            or_tool.EleJetORT.SlidingDRC1 = conf.BoostedElectronC1
        if conf.BoostedElectronC2 > 0:
            or_tool.EleJetORT.SlidingDRC2 = conf.BoostedElectronC2
        if conf.BoostedElectronMaxConeSize > 0:
            or_tool.EleJetORT.SlidingDRMaxCone = conf.BoostedElectronMaxConeSize

    or_tool.MuJetORT.UseSlidingDR = conf.DoBoostedMuon
    if conf.DoBoostedMuon:
        if conf.BoostedMuonC1 > 0:
            or_tool.MuJetORT.SlidingDRC1 = conf.BoostedMuonC1
        if conf.BoostedMuonC2 > 0:
            or_tool.MuJetORT.SlidingDRC2 = conf.BoostedMuonC2
        if conf.BoostedMuonMaxConeSize > 0:
            or_tool.MuJetORT.SlidingDRMaxCone = conf.BoostedMuonMaxConeSize

    if not conf.ElBjet:
        or_tool.EleJetORT.BJetLabel = ""

    if not conf.MuBjet:
        or_tool.MuJetORT.BJetLabel = ""

    or_tool.MuJetORT.UseGhostAssociation = conf.DoMuonJetGhostAssociation

    or_tool.MuJetORT.ApplyRelPt = conf.MuJetApplyRelPt
    if conf.MuJetApplyRelPt:
        if conf.MuJetPtRatio > 0:
            or_tool.MuJetORT.MuJetPtRatio = conf.MuJetPtRatio
        if conf.MuJetTrkPtRatio > 0:
            or_tool.MuJetORT.MuJetTrkPtRatio = conf.MuJetTrkPtRatio
    if conf.MuJetInnerDR > 0:
        or_tool.MuJetORT.InnerDR = conf.MuJetInnerDR

    or_tool.EleMuORT.RemoveCaloMuons = conf.RemoveCaloMuons
    or_tool.EleMuORT.UseDRMatching = conf.ElMu

    if conf.DoFatJets:
        if conf.EleFatJetDR > 0:
            or_tool.EleFatJetORT.DR = conf.EleFatJetDR
        if conf.JetFatJetDR > 0:
            or_tool.JetFatJetORT.DR = conf.JetFatJetDR

    or_tool.EleJetORT.EnableUserPriority = True
    or_tool.MuJetORT.EnableUserPriority = True
    if conf.DoTau:
        or_tool.TauJetORT.EnableUserPriority = True
    if conf.DoPhoton:
        or_tool.PhoJetORT.EnableUserPriority = True

    if conf.PhotonFavoured:
        or_tool.PhoEleORT.SwapContainerPrecedence = True
        or_tool.PhoMuORT.SwapContainerPrecedence = True

    if not conf.EleJet:
        or_tool.EleJetORT.OuterDR = -1.
        or_tool.EleJetORT.SlidingDRMaxCone = -1.

    if not conf.MuonJet:
        or_tool.MuJetORT.OuterDR = -1.
        or_tool.MuJetORT.SlidingDRMaxCone = -1.

    # Remove the view algs
    for container in ("electrons", "muons", "jets", "taus", "photons", "fatjets"):
        try:
            delattr(sequence, "OverlapRemovalViewMaker_"+container+postfix)
        except AttributeError:
            # Attribute error means that that container was never added
            pass

    return sequence
