from TauAnalysisAlgorithms.TauAnalysisSequence import makeTauAnalysisSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool


def baseline_tau_sequence(data_type, configuration, postfix="baseline"):
    # Extract the configuration
    conf = configuration.group("TauBaseline")

    sequence = makeTauAnalysisSequence(
        data_type,
        workingPoint=conf.Id,
        postfix=postfix,
        shallowViewOutput=False,
        deepCopyOutput=False,
        rerunTruthMatching=configuration.Tau.DoTruthMatching,
    )

    if postfix:
        postfix = "_" + postfix

    if conf.ConfigPath != "default":
        getattr(
            sequence, "TauSelectionAlg" + postfix
        ).selectionTool.ConfigPath = conf.ConfigPath
    if configuration.Tau.PrePtCut > 0:
        alg = createAlgorithm("CP::AsgSelectionAlg", "TauPtCutAlg" + postfix)
        addPrivateTool(alg, "selectionTool", "CP::AsgPtEtaSelectionTool")
        alg.selectionTool.minPt = configuration.Tau.PrePtCut
        alg.selectionDecoration = "selectTauPt" + postfix
        sequence.insert(
            0,
            alg,
            inputPropName="particles",
            outputPropName="particlesOut",
            stageName="selection",
            metaConfig={
                "selectionDecorNames": [alg.selectionDecoration],
                # "selectionDecorNamesOutput": [alg.selectionDecoration],
                "selectionDecorCount": [1],
            },
            dynConfig={
                "preselection": lambda meta: "&&".join(meta["selectionDecorNames"])
            },
        )

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "TauSummarySelection" + postfix
    )
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "baseline,as_char"
    sequence.append(
        summary_alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        dynConfig={
            "selectionTool.selectionFlags": lambda meta: [
                name + (",as_bits" if "," not in name else "")
                for name in meta["selectionDecorNames"]
            ]
        },
    )

    return sequence


def signal_tau_sequence(data_type, configuration, postfix="signal"):
    conf = configuration.group("Tau")

    sequence = makeTauAnalysisSequence(
        data_type,
        workingPoint=conf.Id,
        postfix=postfix,
        shallowViewOutput=False,
        deepCopyOutput=False,
        # NB: We ran truth matching in the baseline sequence if we needed to
        rerunTruthMatching=False,
    )
    sequence.removeStage("calibration")

    if postfix:
        postfix = "_" + postfix

    if conf.ConfigPath != "default":
        getattr(
            sequence, "TauSelectionAlg" + postfix
        ).selectionTool.ConfigPath = conf.ConfigPath

    alg = createAlgorithm("CP::AsgSelectionAlg", "TauPtEtaCutAlg" + postfix)
    addPrivateTool(alg, "selectionTool", "CP::AsgPtEtaSelectionTool")
    alg.selectionTool.minPt = conf.Pt
    alg.selectionTool.maxEta = conf.Eta
    alg.selectionDecoration = "selectTauPtEta" + postfix
    sequence.append(
        alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        stageName="selection",
        metaConfig={
            "selectionDecorNames": [alg.selectionDecoration],
            # "selectionDecorNamesOutput": [alg.selectionDecoration],
            "selectionDecorCount": [1],
        },
        dynConfig={"preselection": lambda meta: "&&".join(meta["selectionDecorNames"])},
    )

    # Add a selection tool to set a summary flag
    summary_alg = createAlgorithm(
        "CP::AsgSelectionAlg", "TauSummarySelection" + postfix
    )
    addPrivateTool(summary_alg, "selectionTool", "CP::AsgFlagSelectionTool")
    summary_alg.selectionDecoration = "signal,as_char"
    sequence.append(
        summary_alg,
        inputPropName="particles",
        outputPropName="particlesOut",
        dynConfig={
            "selectionTool.selectionFlags": lambda meta: [
                name + (",as_bits" if "," not in name else "")
                for name in meta["selectionDecorNames"]
            ]
        },
    )

    return sequence
