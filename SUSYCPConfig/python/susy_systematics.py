kinematics = {
    "electrons" : [
        "EG_RESOLUTION.*",
        "EF_SCALE.*",
    ],
    "photons": [
        "EG_RESOLUTION.*",
        "EF_SCALE.*",
    ],
    "jets" : [
        "JET_BJES_Response.*",
        "JET_CombMass.*",
        "JET_EffectiveNP.*",
        "JET_EtaIntercalibration.*",
        "Jet_Flavor.*",
        "JET_JER.*",
        "JET_LargeR.*",
        "JET_MassRes.*",
        "JET_Pileup.*",
        "JET_PunchThrough.*",
        "JET_Rtrk.*",
        "JET_SingleParticle.*",
    ],
    "muons" : [
        "MUON_ID.*",
        "MUON_MS.*",
        "MUON_SAGITTA.*",
        "MUON_SCALE.*",
    ],
    "taus" : [
        "TAUS_TRUEHADTAU_SME_TES.*",
    ],
}
# Don't include photons here as they are identical to electrons
kinematics["all"] = kinematics["electrons"] + kinematics["jets"] + kinematics["muons"] + kinematics["taus"]
