from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
from AsgAnalysisAlgorithms.OverlapAnalysisSequence import makeOverlapAnalysisSequence
from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
from EgammaAnalysisAlgorithms.PhotonAnalysisSequence import makePhotonAnalysisSequence
from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
from TauAnalysisAlgorithms.TauAnalysisSequence import makeTauAnalysisSequence
from FTagAnalysisAlgorithms.FTagAnalysisSequence import makeFTagAnalysisSequence
from MetAnalysisAlgorithms.MetAnalysisSequence import makeMetAnalysisSequence

data_type = "mc"

top_seq = AlgSequence("TopSequence")

# Filter out events without primary vertices
top_seq += createAlgorithm("CP::VertexSelectionAlg",
                           "PrimaryVertexSelectionAlg")
top_seq.PrimaryVertexSelectionAlg.VertexContainer = "PrimaryVertices"
top_seq.PrimaryVertexSelectionAlg.MinVertices = 1

# Set up the systematics loader algorithm
top_seq += createAlgorithm("CP::SysListLoaderAlg", "SysLoaderAlg")
top_seq.SysLoaderAlg.sigmaRecommended = 1

jet_collection = "AntiKt4EMPFlowJets"
btag_training = "201810"
bjet_sc_name = "{}_BTagging{}".format(jet_collection, btag_training)
# # Add links back from the jet b-tagging shallow copy to the original
bjet_ool_alg = createAlgorithm(
    "CP::AsgOriginalObjectLinkAlg", "{}OOLAlg".format(bjet_sc_name))
bjet_ool_alg.particles = bjet_sc_name
bjet_ool_alg.particlesOut = "AntiKt4EMPFlowJets_OOL"
bjet_ool_alg.baseContainerName = jet_collection
bjet_ool_alg.particlesRegex = "^$"
top_seq += bjet_ool_alg

# Make the sequence that calculates the PRW
prw_seq = makePileupAnalysisSequence(data_type)
prw_seq.configure(inputName='EventInfo', outputName='EventInfo_%SYS%')
top_seq += prw_seq

# Make the electron analysis sequence
ele_seq = makeElectronAnalysisSequence(
    data_type, workingPoint="LooseLH.FCLoose", recomputeLikelihood=False)
ele_seq.configure(inputName="Electrons", outputName="AnalysisElectrons_%SYS%")
top_seq += ele_seq

# Make the photon analysis sequence - doesn't work on derivations... the fudge
# factor tool is scheduled :(
photon_seq = makePhotonAnalysisSequence(
    data_type, workingPoint="Tight.FixedCutTight", recomputeIsEM=False)
# The fudge factor tool is scheduled, which doesn't work on derivations
del photon_seq.PhotonShowerShapeFudgeAlg
photon_seq.configure(inputName="Photons", outputName="AnalysisPhotons_%SYS%")
top_seq += photon_seq

# Make the muon analysis sequence
muon_seq = makeMuonAnalysisSequence(data_type, workingPoint="Medium.NonIso")
muon_seq.configure(inputName="Muons", outputName="AnalysisMuons_%SYS%")
top_seq += muon_seq

# Make the jet analysis sequence
jet_seq = makeJetAnalysisSequence(data_type, "AntiKt4EMPFlowJets")
# The f-tag analysis sequence is special - it extends the jet sequence rather than
# making a new one
makeFTagAnalysisSequence(
        jet_seq, data_type, "AntiKt4EMPFlowJets_BTagging201810",  btagWP="FixedCutBEff_77", btagger="DL1")
jet_seq.configure(inputName=bjet_ool_alg.particlesOut, outputName="AnalysisJets_%SYS%")
top_seq += jet_seq
# Remove the 'as_char' part
btag_name = jet_seq.FTagSelectionAlgDL1FixedCutBEff_77.selectionDecoration.rpartition(",")[0]

# Make the tau analysis sequence
tau_seq = makeTauAnalysisSequence(data_type, workingPoint="Tight")
tau_seq.configure(inputName="TauJets", outputName="AnalysisTaus_%SYS%")
top_seq += tau_seq

# Perform overlap removal
OR_seq = makeOverlapAnalysisSequence(data_type, doMuPFJetOR=True, doPhotons=False)
OR_seq.configure(
    inputName={
        "electrons": "AnalysisElectrons_%SYS%",
        "photons": "AnalysisPhotons_%SYS%",
        "muons": "AnalysisMuons_%SYS%",
        "jets": "AnalysisJets_%SYS%",
        "taus": "AnalysisTaus_%SYS%",
    },
    outputName={
        "electrons": "AnalysisElectronsOR_%SYS%",
        "photons": "AnalysisPhotonsOR_%SYS%",
        "muons": "AnalysisMuonsOR_%SYS%",
        "jets": "AnalysisJetsOR_%SYS%",
        "taus": "AnalysisTausOR_%SYS%",
    },
    affectingSystematics={
        "electrons": ele_seq.affectingSystematics(),
        "photons": photon_seq.affectingSystematics(),
        "muons": muon_seq.affectingSystematics(),
        "jets": jet_seq.affectingSystematics(),
        "taus": tau_seq.affectingSystematics()
    }
)
top_seq += OR_seq

# Rebuild the MET
met_seq = makeMetAnalysisSequence(data_type, "AntiKt4EMPFlow", useFJVT=False, treatPUJets=False)
met_seq.configure(
    inputName={
        "electrons": "AnalysisElectrons_%SYS%",
        "photons": "AnalysisPhotons_%SYS%",
        "muons": "AnalysisMuons_%SYS%",
        "jets": "AnalysisJets_%SYS%",
        "taus": "AnalysisTaus_%SYS%",
    },
    outputName="AnalysisMET_%SYS%",
    affectingSystematics={
        "electrons": ele_seq.affectingSystematics(),
        "photons": photon_seq.affectingSystematics(),
        "muons": muon_seq.affectingSystematics(),
        "jets": jet_seq.affectingSystematics(),
        "taus": tau_seq.affectingSystematics()
    }
)
top_seq += met_seq

# Now retrieve the signal objects
signal_ele_seq = makeElectronAnalysisSequence(
    data_type, workingPoint="Tight.FCTight", recomputeLikelihood=False, postfix="signal")
signal_ele_seq.removeStage("calibration")
signal_ele_seq.configure(inputName="AnalysisElectronsOR_%SYS%", outputName="SignalElectrons_%SYS%",
                         affectingSystematics=OR_seq.affectingSystematics("electrons"))
top_seq += signal_ele_seq

signal_photon_seq = makePhotonAnalysisSequence(
    data_type, workingPoint="Tight.FixedCutTight", recomputeIsEM=False, postfix="signal")
signal_photon_seq.removeStage("calibration")
signal_photon_seq.configure(inputName="AnalysisPhotonsOR_%SYS%", outputName="SignalPhotons_%SYS%",
                            affectingSystematics=OR_seq.affectingSystematics("photons"))
top_seq += signal_photon_seq

signal_muon_seq = makeMuonAnalysisSequence(data_type, workingPoint="Tight.Iso", postfix="signal")
signal_muon_seq.removeStage("calibration")
signal_muon_seq.configure(inputName="AnalysisMuonsOR_%SYS%", outputName="SignalMuons_%SYS%",
                          affectingSystematics=OR_seq.affectingSystematics("muons"))
top_seq += signal_muon_seq

signal_tau_seq = makeTauAnalysisSequence(data_type, workingPoint="Tight", postfix="signal")
signal_tau_seq.removeStage("calibration")
signal_tau_seq.configure(inputName="AnalysisTausOR_%SYS%", outputName="SignalTaus_%SYS%",
                         affectingSystematics=OR_seq.affectingSystematics("taus"))
top_seq += signal_tau_seq

# Create the tree
tree_name = "TestTree"
tree_maker = createAlgorithm("CP::TreeMakerAlg", "TreeMaker")
tree_maker.TreeName = tree_name
top_seq += tree_maker

tree_builder = createAlgorithm("CP::AsgxAODNTupleMakerAlg", "TreeBuilder")
tree_builder.TreeName = tree_name
tree_builder.Branches = [
        "EventInfo.runNumber -> runNumber",
        "EventInfo.eventNumber -> eventNumber",
        "SignalElectrons_%SYS%.pt -> %SYS%_ele_pt",
        "SignalElectrons_%SYS%.eta -> %SYS%_ele_eta",
        "SignalElectrons_%SYS%.phi -> %SYS%_ele_phi",
        "SignalMuons_%SYS%.pt -> %SYS%_muon_pt",
        "SignalMuons_%SYS%.eta -> %SYS%_muon_eta",
        "SignalMuons_%SYS%.phi -> %SYS%_muon_phi",
        "SignalPhotons_%SYS%.pt -> %SYS%_photon_pt",
        "SignalPhotons_%SYS%.eta -> %SYS%_photon_eta",
        "SignalPhotons_%SYS%.phi -> %SYS%_photon_phi",
        "SignalTaus_%SYS%.pt -> %SYS%_tau_pt",
        "SignalTaus_%SYS%.eta -> %SYS%_tau_eta",
        "SignalTaus_%SYS%.phi -> %SYS%_tau_phi",
        "AnalysisJetsOR_%SYS%.pt -> %SYS%_jet_pt",
        "AnalysisJetsOR_%SYS%.eta -> %SYS%_jet_eta",
        "AnalysisJetsOR_%SYS%.phi -> %SYS%_jet_phi",
        "AnalysisJetsOR_%SYS%.m -> %SYS%_jet_m",
        "AnalysisJetsOR_%SYS%.{} -> %SYS%_jet_isbjet".format(btag_name),
        ]
tree_builder.systematicsRegex = ".*"
top_seq += tree_builder

tree_filler = createAlgorithm("CP::TreeFillerAlg", "TreeFiller")
tree_filler.TreeName = tree_name
top_seq += tree_filler

print top_seq
