SUSY CP Config
==============

This package is intended mainly as a demonstrator that the CP algorithms can be used to get at least a similar output to most standard SUSYTools configurations.
It has not been fully validated (in particular, weights have not been tested at all) so there will likely be differences.

Right now, the package contains two job options: `SUSYCPConfig/cp_jobo.py` which runs a job using the CP algorithms and `SUSYCPConfig/susy_jobo.py` which runs a job using SUSYTools.
Both job options will output trees containing pt/eta/phi/signal decorations for all objects passing overlap removal.

The CP algorithms approach is controlled by the code in the python modules, and the SUSY approach uses the `ApplySUSYToolsAlg`.
There is currently also a `METWriterAlg` meant for writing MET containers to output trees, but this is not currently used (and MET is simply not written).

This should be able to take and use most 'normal' SUSYTools configuration files.

Major things missing
--------------------

- This always runs all objects (jets, electrons, photons, muons + taus) even though not all analyses will use all objects.
  This means that things like overlap removal and MET will behave differently compared to code not doing this
- Large-R jets have not been included at all
- Weights are not calculated, but there aren't large reasons not to.